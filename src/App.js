import React, {Fragment} from 'react';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import Dashboard from './pages/admin/Dashboard';
import Home from './pages/Home';

function App() {
  return(
    <Fragment>
      <Router>
        <Routes>
        <Route path='/' exact element={<Home/>} />
         <Route path='/login' exact element={<Dashboard/>} />
         <Route path='/register' exact element={<formulario />} />
        </Routes>
      </Router>
    </Fragment>
  );
}

export default App;