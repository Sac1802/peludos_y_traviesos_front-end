import React from 'react'
import Navbar from '../../components/Navbar'
import Sidebar from '../../components/Sidebar'


function Dashboard() {
  return (
    
    <div className='wrapper'>

    <Navbar></Navbar>
    <Sidebar></Sidebar>

    </div>

    
  )
}

export default Dashboard